"""
Django settings for  project.
"""
from datetime import date, datetime, timezone
from pathlib import Path

from apps.utils import gettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
)


CONSTANCE_CONFIG = {
    'SCHOOL_ENROLMENT_DUE_DAY': (
        date(datetime.now(tz=timezone.utc).date().year, 9, 30), _('Due day for first day at school')),
    'REPORT_HEADER': ('', _('Report header')),
    'INCOME_LIMIT': (1666, _('Income limit'))
}

# HERE STARTS DYNACONF EXTENSION LOAD (Keep at the very bottom of settings.py)
# Read more at https://dynaconf.readthedocs.io/en/latest/guides/django.html
import dynaconf  # noqa

settings = dynaconf.DjangoDynaconf(
    envvar_prefix='MONTEHELPER',
    settings_files=["settings.yaml", ".secrets.yaml"],
    environments=True,
    env_switcher='MONTEHELPER_ENV',
    env='default'
)  # noqa
# HERE ENDS DYNACONF EXTENSION LOAD (No more code below this line)
