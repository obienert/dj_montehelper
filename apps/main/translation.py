from modeltranslation.translator import register, TranslationOptions

from apps.main.models import IncomeType


@register(IncomeType)
class IncomeTypeTranslationOptions(TranslationOptions):
    fields = ('name',)
