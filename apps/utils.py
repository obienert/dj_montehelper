from dynaconf.utils.parse_conf import Lazy


def make_translation(value, **context):
    from django.utils.translation import gettext_lazy
    value = gettext_lazy(value)
    return value


gettext_lazy = lambda value: Lazy(value, formatter=make_translation)
