from django.contrib.auth import authenticate, login, logout
from django.middleware.csrf import get_token
from drf_spectacular.utils import extend_schema, inline_serializer, OpenApiParameter
from rest_framework import serializers
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView


@extend_schema(
    responses={
        200: inline_serializer(
            name='CSRF-Token',
            fields={
                'detail': serializers.CharField(),
            }
        )
    },
    parameters=[
        OpenApiParameter(
            name='X-CSRFToken',
            type=str,
            location=OpenApiParameter.HEADER,
            response=True,
        )
    ]
)
@api_view()
@permission_classes([AllowAny])
def get_csrf(request):
    response = Response({'detail': 'CSRF cookie set'})
    response['X-CSRFToken'] = get_token(request)
    return response


@extend_schema(
    request=inline_serializer(
        name='Login Request',
        fields={
            'username': serializers.CharField(),
            'password': serializers.CharField()
        }
    ),
    responses={
        200: inline_serializer(
            name='Login Success',
            fields={
                'detail': serializers.CharField(),
            }
        ),
        400: inline_serializer(
            name='Login Failure',
            fields={
                'detail': serializers.CharField(),
            }
        )
    }
)
@api_view(['POST'])
@permission_classes([AllowAny])
def login_view(request):
    data = request.data
    username = data.get('username')
    password = data.get('password')
    if username is None or password is None:
        return Response({'detail': 'Please provide username and password.'}, status=400)
    user = authenticate(username=username, password=password)
    if user is None:
        return Response({'detail': 'Invalid credentials.'}, status=400)
    login(request, user)
    return Response({'detail': 'Successfully logged in.'})


@extend_schema(
    responses={
        200: inline_serializer(
            name='Logout Success',
            fields={
                'detail': serializers.CharField(),
            }
        ),
        400: inline_serializer(
            name='Logout Failure',
            fields={
                'detail': serializers.CharField(),
            }
        )
    }
)
@api_view()
def logout_view(request):
    if not request.user.is_authenticated:
        return Response({'detail': 'You\'re not logged in.'}, status=400)

    logout(request)
    return Response({'detail': 'Successfully logged out.'})


@extend_schema(
    responses={
        200: inline_serializer(
            name='QueryAuthStatus',
            fields={
                'detail': serializers.BooleanField(),
            }
        )
    }
)
class SessionView(APIView):
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        return Response({'isAuthenticated': True})


@extend_schema(
    responses={
        200: inline_serializer(
            name='WhoAmI',
            fields={
                'detail': serializers.CharField(),
            }
        )
    }
)
class WhoAmIView(APIView):
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get(request):
        return Response({'username': request.user.username})
