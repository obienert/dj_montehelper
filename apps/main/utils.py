def find_upper_neighbour(value, df, col_name):
    exact_match = df[df[col_name] == value]
    if not exact_match.empty:
        return exact_match.values[0]
    else:
        values = df[df[col_name] > value]
        upper_neighbour_ind = values[col_name].idxmin() if not values.empty else df[col_name].idxmax()
        res = list(df.iloc[upper_neighbour_ind])
        return res


def get_list_value_at_index_or_last(lst, i):
    return lst[i] if len(lst) > i else lst[-1]
