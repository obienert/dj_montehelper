from django.utils import timezone
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from apps.main.models import Adult, Child, Address, AdultChild, ClubMembership


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class ChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Child
        fields = '__all__'


class AdultSerializer(serializers.ModelSerializer):
    address = serializers.StringRelatedField()
    partner = serializers.StringRelatedField()
    club_member = SerializerMethodField(method_name='is_club_member')

    class Meta:
        model = Adult
        fields = ('id', 'last_name', 'first_name', 'birth_date', 'club_member', 'staff', 'household_size', 'no_kiga_fee',
                  'address', 'partner')

    @staticmethod
    def is_club_member(instance):
        return check_membership(instance)


class PartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Adult
        fields = ('id', 'last_name', 'first_name')


class AdultChildSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='child.id')
    last_name = serializers.ReadOnlyField(source='child.last_name')
    first_name = serializers.ReadOnlyField(source='child.first_name')

    class Meta:
        model = AdultChild
        fields = ('id', 'last_name', 'first_name', 'kinship', 'liable')


class ChildAdultSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='adult.id')
    last_name = serializers.ReadOnlyField(source='adult.last_name')
    first_name = serializers.ReadOnlyField(source='adult.first_name')

    class Meta:
        model = AdultChild
        fields = ('id', 'last_name', 'first_name', 'kinship')


class AdultRetrieveSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    partner = PartnerSerializer()
    children = AdultChildSerializer(source='adultchild_set', many=True)
    club_member = SerializerMethodField(method_name='is_club_member')

    class Meta:
        model = Adult
        fields = ('last_name', 'first_name', 'birth_date', 'club_member', 'staff', 'household_size', 'no_kiga_fee',
                  'address', 'partner', 'children')

    @staticmethod
    def is_club_member(instance):
        return check_membership(instance)


class ChildRetrieveSerializer(serializers.ModelSerializer):
    parents = ChildAdultSerializer(source='adultchild_set', many=True)

    class Meta:
        model = Child
        fields = ('last_name', 'first_name', 'birth_date', 'birth_place', 'care_time',
                  'kita', 'parents')


def check_membership(instance):
    # noinspection PyBroadException
    try:
        now = timezone.now()
        return ClubMembership.objects \
            .filter(adult=instance.id) \
            .filter(period__contains=now) \
            .exists()
    except Exception:
        return False
