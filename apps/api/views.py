from rest_framework import viewsets

from apps.api.serializers import AdultSerializer, ChildSerializer, AdultRetrieveSerializer, ChildRetrieveSerializer
from apps.main.models import Adult, Child


class AdultViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing adults.
    """
    queryset = Adult.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return AdultRetrieveSerializer
        return AdultSerializer


class ChildViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing children.
    """
    queryset = Child.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ChildRetrieveSerializer
        return ChildSerializer
