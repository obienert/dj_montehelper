from django.urls import path, include
from rest_framework import routers

from . import views, auth_views

router = routers.DefaultRouter()
router.register(r'adults', views.AdultViewSet)
router.register(r'children', views.ChildViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('csrf/', auth_views.get_csrf, name='api-csrf'),
    path('login/', auth_views.login_view, name='api-login'),
    path('logout/', auth_views.logout_view, name='api-logout'),
    path('session/', auth_views.SessionView.as_view(), name='api-session'),
    path('whoami/', auth_views.WhoAmIView.as_view(), name='api-whoami'),
]
