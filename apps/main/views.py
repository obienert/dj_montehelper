import functools
import ssl
from os.path import join

from django.conf import settings
from django.templatetags.static import static
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django_weasyprint import WeasyTemplateResponse, WeasyTemplateResponseMixin
from django_weasyprint.utils import django_url_fetcher

from apps.main.decorators import process_report_response
from apps.main.fees import Fee
from apps.main.models import Adult


@method_decorator(process_report_response, name='dispatch')
class ParentFeeView(TemplateView):
    template_name = "main/parent_fee.html"

    def get_context_data(self, **kwargs):
        adult = Adult.objects.get(pk=kwargs['pk'])
        context = super(ParentFeeView, self).get_context_data(**kwargs)

        fee = Fee(adult)
        data = {
            'payer': fee.payer,
            'adults': fee.adult_calculations,
            'children': fee.children_calculations
        }
        meta = {
            'person': fee.person,
            'type': 'parental_contribution'
        }
        context['data'] = data
        context['meta'] = meta
        return context


class CustomWeasyTemplateResponse(WeasyTemplateResponse):
    # customized response class to change the default URL fetcher
    def get_url_fetcher(self):
        # disable host and certificate check
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        return functools.partial(django_url_fetcher, ssl_context=context)


class ParentFeePrintView(WeasyTemplateResponseMixin, ParentFeeView):
    # output of MyModelView rendered as PDF with hardcoded CSS
    pdf_stylesheets = [
        join(settings.BASE_DIR, static('css/fee.css').lstrip('/')),
    ]
    #
    # show pdf in-line (default: True, show download dialog)
    pdf_attachment = False
    # custom response class to configure url-fetcher
    response_class = CustomWeasyTemplateResponse
