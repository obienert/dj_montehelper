grp.jQuery(function() {
    grp.jQuery('.hasDatepicker').datepicker('option','firstDay', 1);
    grp.jQuery('.hasDatepicker').datepicker('option','changeYear', true);
    grp.jQuery('.hasDatepicker').datepicker('option', 'yearRange', '-100:+0');
});