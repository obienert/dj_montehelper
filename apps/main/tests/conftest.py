import pytest
import pandas as pd
from pytest_factoryboy import register, LazyFixture

from . import factories

register(factories.AddressFactory)
register(factories.PersonFactory)
register(factories.AdultFactory, partner=LazyFixture('adult_2'))
register(factories.AdultFactory, 'adult_2', last_name='Amarth', first_name='Amon')
register(factories.AdultFactory, 'adult_3', last_name='Peppone', first_name='Beppo')
register(factories.ChildFactory)
register(factories.ChildFactory, 'child_2', last_name='Petterson', first_name='Findus')
register(factories.IncomeTypeFactory)
register(factories.IncomeFactory, adult=LazyFixture('adult'))
register(factories.IncomeFactory, 'income_2', adult=LazyFixture('adult_2'))
register(factories.EmailAddressFactory)
register(factories.PhoneNumberFactory)
register(factories.RulingFactory)
register(factories.AdultChildFactory)
register(factories.AdultChildFactory, 'adult_child_2', adult=LazyFixture('adult_2'),
         kinship='mother', payer=False)
register(factories.AdultChildFactory, 'adult_child_3', adult=LazyFixture('adult_3'), child=LazyFixture('child'))
register(factories.AdultChildFactory, 'adult_child_4', adult=LazyFixture('adult_3'), child=LazyFixture('child_2'))


@pytest.fixture
def df_school_fee():
    lst = []
    income = 1000
    for _ in range(25):
        fee = income / 10
        fee2 = fee * .8
        fee3 = fee * .6
        lst.append((income, fee, fee2, fee3,))
        income = income + 200
    columns = ['income', '1', '2', '3']
    return pd.DataFrame(lst, columns=columns)
