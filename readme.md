# Django-Montehelper

## Installation

Clone the repository.

Set up a virtual environment with python >= 3.9, then activate it.

Change into the root directory, then install requirements with
```shell
pip install -r requirements.txt
```
This project uses [pip-tools](https://github.com/jazzband/pip-tools), so please edit *requirements.in* 
when adding or removing packages, then run:
```shell
pip-compile
pip-sync
```

Create a superuser:
```shell
./manage.py migrate
./manage.py createsuperuser
```
## Start up
Run the django-server with:
```shell
./manage.py runserver 8000
```

Run the tests:
```shell
pytest
```

# REST-API
This project uses [drf-spectacular](https://github.com/tfranzel/drf-spectacular/) to generate an OpenAPI 3 aware schema.
After logging in, the schema can be reached by `http://127.0.0.1:8000/schema/ui/`.