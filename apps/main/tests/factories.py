import factory
from datetime import datetime, date

from apps.main.models import Address, Person, Child, Adult, AdultChild, Income, PhoneNumber, EmailAddress, Ruling, \
    IncomeType

currentDateTime = datetime.now()
current_year = currentDateTime.date().year


class AddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Address

    street = 'Street'
    house_number = '1'
    zip_code = '12345'
    city = 'City'


class PersonFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Person

    last_name = 'Hands'
    first_name = 'Idle'
    birth_date = date(current_year-1, 1, 1)


class ChildFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Child

    last_name = 'Kilmister'
    first_name = 'Lemmy'
    birth_date = date(current_year-10, 1, 1)
    birth_place = 'place'
    care_time = 2
    kita = False


class AdultFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Adult

    last_name = 'Raven'
    first_name = 'Count'
    birth_date = date(current_year-35, 1, 1)
    iban = "DE123"
    address = factory.SubFactory(AddressFactory)
    club_member = True
    staff = False
    household_size = 5
    partner = None
    no_kiga_fee = False


class AdultChildFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AdultChild

    adult = factory.SubFactory(AdultFactory)
    child = factory.SubFactory(ChildFactory)
    kinship = AdultChild.FATHER
    liable = True
    max_contribution = False
    payer = True


class IncomeTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = IncomeType

    name = 'Salary'


class IncomeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Income

    amount = 1234
    type = factory.SubFactory(IncomeTypeFactory)
    adult = factory.SubFactory(AdultFactory)


class PhoneNumberFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PhoneNumber

    phone_number = "+49123456789"
    adult = factory.SubFactory(AdultFactory)


class EmailAddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = EmailAddress

    email_address = "a@bc.de"
    type = EmailAddress.WORK
    adult = factory.SubFactory(AdultFactory)


class RulingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Ruling

    type = Ruling.EXTENDED_DAY_TIME
    valid_from = date(2020, 1, 1)
    valid_to = date(2021, 1, 1)
    child = factory.SubFactory(ChildFactory)
